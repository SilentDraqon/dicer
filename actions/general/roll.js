const getUserFromMention = require("../../modules/getUserFromMention")
const randomNumber = require("../../modules/randomNumber")
const diceMath = require("../../modules/diceMath")
const { AttachmentBuilder } = require('discord.js');
var File = require("../../modules/fileschema")
const mongo = require("../../modules/mongo")
var mongoose = require('mongoose');

function GetRandomIndex(anyArr){
    return Math.floor(Math.random() * (anyArr.length));
}

function diceSplitter(rolled_dice){
	return rolled_dice.split("d")
}

function addSplitter(rolled_dice){
	return rolled_dice.split("+")
}

function arrayAdder(answer, addend){
	let arrayAdder = answer
	arrayAdder.push(addend)
}

function totalAdder(answer, addend){
	return answer +++ addend
}

module.exports = {
	name: 'roll',
	description: 'randomly rolls a number of given sided dice. Eg: 1d20 rolls a 20 \n(number_of_dice + d + number_of_sides)',
	async execute(message, prefix, args, command) {		
		
		let sender = message.author.toString() //grabs ping of person who sent the command
		let rolledDice = args[0] //grabs the 1st argument, (which will be the dice roll argument)

		let splitDice = diceSplitter(rolledDice) //splits rolledDice by the d
		let splitProduct = [splitDice[0], splitDice[1]]
		let answer = diceMath(splitDice[0], splitDice[0]) //uses the split array of rolledDice to role acordingly
		let addend = addSplitter(rolledDice)

		let addedAnswer = totalAdder(splitProduct, addend)
		let arrayAnswer = arrayAdder(splitProduct, addend)

		console.log(splitProduct)
		return message.reply( sender + ' rolled ' + rolledDice + " and got " + addedAnswer + "!   proof:(" + splitDice + addend[1] + ")")
	}
};


const getUserFromMention = require("../../modules/getUserFromMention")
const { AttachmentBuilder } = require('discord.js');
var File = require("../../modules/fileschema")
const mongo = require("../../modules/mongo")
var mongoose = require('mongoose');

function GetRandomIndex(anyArr){
    return Math.floor(Math.random() * (anyArr.length));
}

module.exports = {
	name: 'greet',
	description: 'greeets tagged user',
	async execute(message, prefix, args, command) {		
		let mentioned_user = ""
		if (args[0]) mentioned_user = getUserFromMention(args[0]).toString()
		return message.reply("Hey "+mentioned_user+"!")
	}
};


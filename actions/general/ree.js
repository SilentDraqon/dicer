const getUserFromMention = require("../../modules/getUserFromMention")
const randomNumber = require("../../modules/randomNumber")
const diceMath = require("../../modules/diceMath")
const { AttachmentBuilder } = require('discord.js');
var File = require("../../modules/fileschema")
const mongo = require("../../modules/mongo")
var mongoose = require('mongoose');

function GetRandomIndex(anyArr){
    return Math.floor(Math.random() * (anyArr.length));
}

function diceSplitter(rolled_dice){
	return rolled_dice.split("d")
}

function addSplitter(rolled_dice){
	return rolled_dice.split("+")
}

function arrayAdder(diceResult, addend=0){
    const array1 = diceResult

    const initialValue = 0;
    const reducedDice = array1.reduce(
      (accumulator, currentValue) => accumulator + currentValue,
      initialValue
    );

        console.log(reducedDice)
        console.log(addend)

        const integeredAddend = Number(addend)

    return reducedDice + integeredAddend

}

function totalAdder(answer, addend){
	return answer +++ addend
}

module.exports = {
	name: 'ree',
	description: 'randomly rolls a number of given sided dice. Eg: 1d20 rolls a 20 \n(number_of_dice + d + number_of_sides)',
	async execute(message, prefix, args, command) {		
		
        if( args[0].includes("+")){
		    let sender = message.author.toString() //grabs ping of person who sent the command
		    let prompt = args[0] //grabs the 1st argument, (which will be the dice roll argument)
            let splitAddend = addSplitter(prompt)
            let rolledDice = splitAddend[0]

		    let splitDice = diceSplitter(rolledDice) //splits rolledDice by the d
	    	let diceResult = diceMath(splitDice[0], splitDice[1]) //uses the split array of rolledDice to role acordingly

            let answer = arrayAdder(diceResult, splitAddend[1])

    		return message.reply( sender + ' rolled ' + prompt + " and got " + answer + "!   proof:(" + diceResult + ", + " + splitAddend[1] +")")
        } 
        else {
            let sender = message.author.toString() //grabs ping of person who sent the command
		    let prompt = args[0] //grabs the 1st argument, (which will be the dice roll argument)
            let rolledDice = prompt

		    let splitDice = diceSplitter(rolledDice) //splits rolledDice by the d
	    	let diceResult = diceMath(splitDice[0], splitDice[1]) //uses the split array of rolledDice to role acordingly

            let answer = arrayAdder(diceResult, 0)

    		return message.reply( sender + ' rolled ' + prompt + " and got " + answer + "!   proof:(" + diceResult +")")
        }
    }
};


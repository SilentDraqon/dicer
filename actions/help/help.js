const path = require("path");
const fs = require("fs");

function getDirectories(path) {
	return fs.readdirSync(path).filter(function (file) {
		return fs.statSync(path+'/'+file).isDirectory();
	});
}

module.exports = {
	name: 'help',
	description: 'shows helps for all commands',
	execute(message) {
		let all_actions = ""
		for (let i=0; i<getDirectories(path.join(__dirname, '../')).length; i++) {
			const actionsPath = path.join(__dirname, '../'+getDirectories(path.join(__dirname, '../'))[i]);
			const actionFiles = fs.readdirSync(actionsPath).filter(file => file.endsWith('.js'));
			console.log(getDirectories(path.join(__dirname, '../')).length)
			all_actions = all_actions + "\n\n**" + getDirectories(path.join(__dirname, '../'))[i] + " Actions**:"
			for (const file of actionFiles) {
				const filePath = path.join(actionsPath, file);
				const action = require(filePath);
				all_actions = all_actions + "\n**" + action.name + "** - " + action.description
			}
		}
		message.reply( all_actions );
	},
};
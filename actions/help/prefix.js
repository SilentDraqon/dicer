const fs = require('fs');

module.exports = {
	name: 'prefix',
	description: 'shows or changes the prefix',
	execute(message, prefix, args, command) {
		if (message.author.bot) return;

		if(args[0]) {
			let json = require("../../settings.json")
			json.prefix = args[0]
			console.log(json)
			fs.writeFileSync("./settings.json", JSON.stringify(json), {encoding:'utf8',flag:'w'})
		}
		message.reply({content: "Hey "+ message.author.toString() +  "! The current prefix is **"+require("../../settings.json").prefix+"** "})
	}
};


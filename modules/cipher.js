const crypto = require('crypto');
const Crypto = require("crypto")
var rsa_priv = require("../keys/rsa_priv")
const algorithm = 'aes-256-ctr';
const ENCRYPTION_KEY = require("../keys/key")
const IV_LENGTH = 16;
var { JSEncrypt } = require('nodejs-jsencrypt')

const createRandomNumber = () => Math.floor(Math.random() * Date.now()).toString(16)

const rsa_decrypt = (encryptedJSON) => {
    const encrypt = new JSEncrypt();
    encrypt.setPrivateKey(rsa_priv)
    return JSON.parse(encrypt.decrypt(encryptedJSON))
}

function aes_encrypt(text) {
    let iv = crypto.randomBytes(IV_LENGTH);
    let cipher = crypto.createCipheriv(algorithm, Buffer.from(ENCRYPTION_KEY, 'hex'), iv);
    let encrypted = cipher.update(text);
    encrypted = Buffer.concat([encrypted, cipher.final()]);
    return iv.toString('hex') + ':' + encrypted.toString('hex');
}

const protectSensitiveFields = (datamodel_instance, data_object = null) => {
    Object.keys(datamodel_instance.getKeys()).forEach((key) => {
      if(datamodel_instance.getKeys()[key].is_sensitive) 
        if (data_object[key]) data_object[key] = aes_encrypt(data_object[key])
        else datamodel_instance[key] = aes_encrypt(datamodel_instance[key])
      })
    return data_object ?? datamodel_instance
  }
  
const generateHashAndSalts = (datamodel_instance) => {
  Object.keys(datamodel_instance.getKeys()).forEach((key) => {
    const salt_hash = generateHashAndSalt(datamodel_instance[key])
    if (datamodel_instance.getKeys()[key].legacy.type === "Password") datamodel_instance[key] = salt_hash.salt + "." + salt_hash.hash
  })
  return datamodel_instance
}

const generateHashAndSalt = (password, salt) => {
  if (!salt) {
    let salt = Crypto.randomBytes(16).toString('hex')
    return {salt: salt, hash: Crypto.pbkdf2Sync(password, salt, 1000, 64, 'sha512').toString('hex')}
  }
  else return {hash: Crypto.pbkdf2Sync(password, salt, 1000, 64, 'sha512').toString('hex')}
}

function aes_decrypt(text) {
    let textParts = text.split(':');
    let iv = Buffer.from(textParts.shift(), 'hex');
    let encryptedText = Buffer.from(textParts.join(':'), 'hex');
    let decipher = crypto.createDecipheriv(algorithm, Buffer.from(ENCRYPTION_KEY, 'hex'), iv);
    let decrypted = decipher.update(encryptedText);
    decrypted = Buffer.concat([decrypted, decipher.final()]);
    return decrypted.toString();
}

function createToken(level) {
    if(level === "1") return createRandomNumber()
    if(level === "2") return createRandomNumber() * 16
    if(level === "3") return createRandomNumber() * 64
}

module.exports = {
    aes_encrypt: aes_encrypt,
    aes_decrypt: aes_decrypt,
    rsa_decrypt: rsa_decrypt,
    protectSensitiveFields: protectSensitiveFields,
    generateHashAndSalts: generateHashAndSalts,
    generateHashAndSalt: generateHashAndSalt,
    createRandomNumber: createRandomNumber,
    createToken: createToken
}
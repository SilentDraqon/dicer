var mongoose = require("mongoose")
var mongo = require("./mongo")

const fileBlueprint = {
    index: {
        legacy: {
            type: "String",
            required: true
        },
    },
    src: {
        legacy: {
            type: "String",
            required: true
        },
        is_sensitive: true
    },
    tags: {
        legacy: {
            type: "Array",
            required: true
        },
        is_sensitive: true
    }
}

const fileMongoScheme = mongo.createMongoScheme(fileBlueprint)
const fileSchema = new mongoose.Schema(fileMongoScheme)

fileSchema.methods.getKeys = function getKeys() {
    return fileBlueprint
}

const File = mongoose.model("files", fileSchema)

module.exports = File
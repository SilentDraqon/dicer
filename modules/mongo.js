var mongoose =  require('mongoose')

const mongo_collection = "Len-Kagamine"
const mongo_servertype = "mongodb+srv"
const mongo_connector = "SilentDraqon:HsmdD6y1fKdV7F8k"
const mongo_cluster = "silentcluster.gnfrz.mongodb.net"
const mongo_settings = "retryWrites=true&w=majority"

function castMongoTypes (str_type) {
    if (str_type === "Number") return Number;
    else if (str_type === "String") return String;
    else if (str_type === "Email") return String;
    else if (str_type === "Password") return String;
    else if (str_type === "Code") return String;
    else if (str_type === "Boolean") return Boolean;
    else if (str_type === "Array") return [String];
}

function createMongoScheme (datamodel_instance) {
    let new_obj = {}
    Object.keys(datamodel_instance).forEach((key) => {
        new_obj[key] = JSON.parse(JSON.stringify(datamodel_instance[key].legacy))
        console.log(new_obj[key])
        new_obj[key].type = castMongoTypes(new_obj[key].type)
    })
    return new_obj
}

function saveItem(model_instance, callback) {
    model_instance.save(() => {callback()})
}

async function deleteItemByIndex(Model, query) {
    await mongoose.connect(mongo_servertype+"://"+mongo_connector+"@"+mongo_cluster+"/"+mongo_collection+"?"+mongo_settings);
    return await Model.deleteOne(query)
}

const defaultBoolToFalse = (model_instance) => {
    Object.keys(model_instance.getKeys()).forEach((key) => {
        if (model_instance.getKeys()[key].type === "Boolean" && !model_instance[key]) model_instance[key] = false
    })
    return model_instance
}

module.exports = {
    uri: mongo_servertype+"://"+mongo_connector+"@"+mongo_cluster+"/"+mongo_collection+"?"+mongo_settings,
    saveItem: saveItem,
    deleteItemByIndex: deleteItemByIndex,
    castMongoTypes: castMongoTypes,
    createMongoScheme: createMongoScheme,
    defaultBoolToFalse: defaultBoolToFalse
}
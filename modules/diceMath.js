const randomNumber = require("./randomNumber")

function diceMath(numberOfRolls, numberOfSides){
	
	let answer = []
	
	for (let step = 0; step < numberOfRolls; step++) {
		answer.push(randomNumber(numberOfSides, 1))
	}
	
	return answer
}

module.exports = diceMath
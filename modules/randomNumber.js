function randomNumber(numberCeiling, numberFloor=0) {
  min = Math.ceil(numberFloor);
  max = Math.floor(numberCeiling);
  return Math.floor(Math.random() * (max - min + 1) + min); 
}

module.exports = randomNumber
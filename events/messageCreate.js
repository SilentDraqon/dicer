const path = require("path");
const fs = require("fs");

module.exports = {
    name:  "messageCreate",
    once: false,
    execute(message) {
        if (message.author.bot) return;
        const prefix = require("../settings.json").prefix;

        // Map all actions
        if (message.content.startsWith(prefix+" ")) {            

            // Get Command
            const commandBody = message.content.slice(prefix.length+1);
            const args = commandBody.split(' ');
            const command = args.shift().toLowerCase();

            // Retrieve all subdirs from a directory
            function getDirectories(path) {
                return fs.readdirSync(path).filter(function (file) {
                    return fs.statSync(path+'/'+file).isDirectory();
                });
            }

            // Set all retrieved commands
            for (let i=0; i<getDirectories(path.join(__dirname, '../actions')).length; i++) {
                const actionsPath = path.join(__dirname, '../actions/'+getDirectories(path.join(__dirname, '../actions'))[i]);
                const actionFiles = fs.readdirSync(actionsPath).filter(file => file.endsWith('.js'));
                for (const file of actionFiles) {
                    const filePath = path.join(actionsPath, file);
                    const action = require(filePath);
                    if (command === action.name) {
                        action.execute(message, prefix, args, command)
                        console.log("Responding with action -> "+ action.name)
                    }
                }
            }   
        }
    }
}
# Len Kagamine

*A Discord Bot made by SilentDraqon*

## Installation Manual for Developers

### Download Backend
1. Go to https://gitlab.com/SilentDraqon/discordbot and clone this repository. (Requires decent knowledge about git and terminals).
2. Unpack the archive you downloaded folder 
3. Delete the archive, but keep the unpacked folder.

### Install Dependencies
1. Use the terminal to navigate into wherever you unpacked the archive to.
2. run *npm install* to install all of its dependencies.

## How to Run the Bot

### The Normal Way

Run ***node bot.js*** while being inside this project in the terminal.
This command will behave like a server,a nd wont stop executing b yi ts own.

### Slash Commands

Discord announced Slash commands in 2022, which you can redeploy using ***node deploy-commands.js***.

## Where to Code?

This bot consists out of three folders, ***actions***, ***commands*** and ***events***. Inside those folders, every file represents a single action, command or event.

### Commands

Commands is short for the newly developed ***Slash Commands API***. I personally rather have a prefix like "uwu" instead of forcing the user into using Slash commands.

Still the folder for slash commands remain here, aswell as the *deploy-commands.js*, even tho using em, ruins a bit of fun for the user, but adds clarity on which commands exist, and how they work xX

### Events
***Events*** are your entrypoint for alotta interactions. You can add logic for every event the discords API offers..

The event ***error*** fires on every error of your app (great for debugging), and ***ready*** when the bot starts (great for getting additional data at bot-boot, if ever needed).

The most important existing event is ***messageCreate.js***, it fires whenever an user sends an message, and responds with something. If you want the bot to listen to a new word, and then respond, you will have to code the ***if (message.content === "something")*** inside that file.

### Actions
***Actions*** 
To make things easier when Interacting with ***messageCreate.js***, i created the actions folder.
Every file should represent an action (like greet).
// Require Libraries
const path = require("path");
const fs = require("fs");
const { Client, Collection } = require('discord.js');
const config = require('./config.json');
const client = require("./modules/client")

console.log(". . . Starting Bot . . .")

// Get all commands this bot uses
const commandsPath = path.join(__dirname, 'commands');
const commandFiles = fs.readdirSync(commandsPath).filter(file => file.endsWith('.js'));
client.commands = new Collection();

// Get all events this bot uses
const eventsPath = path.join(__dirname, 'events');
const eventFiles = fs.readdirSync(eventsPath).filter(file => file.endsWith('.js'));

// Set all retrieved commands
console.log("\n> Setting commands <")
for (const file of commandFiles) {
	const filePath = path.join(commandsPath, file);
	const command = require(filePath);
    console.log("> Setting command -> "+ command.data.name)
	client.commands.set(command.data.name, command);
}

// Retrieve all subdirs from a directory
function getDirectories(path) {
	return fs.readdirSync(path).filter(function (file) {
		return fs.statSync(path+'/'+file).isDirectory();
	});
}

// Set all retrieved commands
console.log("\n> Setting actions <")
for (let i=0; i<getDirectories(path.join(__dirname, 'actions')).length; i++) {
	const actionsPath = path.join(__dirname, 'actions/'+getDirectories(path.join(__dirname, 'actions'))[i]);
	const actionFiles = fs.readdirSync(actionsPath).filter(file => file.endsWith('.js'));
	for (const file of actionFiles) {
		const filePath = path.join(actionsPath, file);
		const action = require(filePath);
		console.log("> Setting "+getDirectories(path.join(__dirname, 'actions'))[i]+" action -> "+ action.name)
		client.commands.set(action.name, action);
	}
}

// Register all retrieved events
console.log("\n> Setting events <")
for (const file of eventFiles) {
	const filePath = path.join(eventsPath, file);
	const event = require(filePath);
    console.log("> Registering event ->" + event.name);
	if (event.once) client.once(event.name, (...args) => event.execute(...args));
	else client.on(event.name, (...args) => event.execute(...args));
}

// Login to Discord with your client's token
client.login(config.token);